//Angela Sposato 1934695
package lab02;

public class BikeStore {
	public static void main(String[] args) {
		Bicycle[] bikes = new Bicycle[4];
		bikes[0] = new Bicycle("Raleigh", 20, 40);
		bikes[1] = new Bicycle("Cervelo", 21, 31);
		bikes[2] = new Bicycle("Kona", 23, 42);
		bikes[3] = new Bicycle("Connondale", 24, 32);
		for (int i = 0; i<bikes.length; i++) {
			System.out.println(bikes[i]);
		}
	}
}
