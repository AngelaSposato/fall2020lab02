//Angela Sposato 1934695
package lab02;

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;

	public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
		this.manufacturer = manufacturer;
		this.numberGears = numberGears;
		this.maxSpeed = maxSpeed;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public int getNumberGears() {
		return numberGears;
	}

	public double getMaxSpeed() {
		return maxSpeed;
	}

	public String toString() {
		return "Manufacturer: " + manufacturer + " Number of gears: " + numberGears + " Max Speed: " + maxSpeed;
	}
}

